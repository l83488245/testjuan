package com.service.javabit.controller;



import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestJavabit {

        JavabitDelegate javabitDelegate = new JavabitDelegate();


    @Test
    public void testhelloworld(){

        String expactReturnValue = "hello"; // You should put the expect String type value here.

        String returnValue = javabitDelegate.helloworld("hello");

        assertEquals(expactReturnValue, returnValue);
    }

}