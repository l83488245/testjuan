package com.service.javabit.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-05-18T06:18:52.825Z")

@RestSchema(schemaId = "javabit")
@RequestMapping(path = "/javabit", produces = MediaType.APPLICATION_JSON)
public class JavabitImpl {

    @Autowired
    private JavabitDelegate userJavabitDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userJavabitDelegate.helloworld(name);
    }

}
