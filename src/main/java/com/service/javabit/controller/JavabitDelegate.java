package com.service.javabit.controller;

import org.springframework.stereotype.Component;


@Component
public class JavabitDelegate {

    public String helloworld(String name){

        // Do Some Magic Here!
        return name;
    }
}
